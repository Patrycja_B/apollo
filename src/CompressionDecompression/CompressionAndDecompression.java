package CompressionDecompression;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JDesktopPane;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.TextArea;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.awt.event.ActionEvent;

public class CompressionAndDecompression extends JFrame {

	private JPanel contentPane;
	private JTextField quantizationLevel;
	static int N = 8;
	static double n = 8.0;
	static double[][] T = new double[N][N];
	static double[][] Tt = new double[N][N];
	static double[][] M ={{16,8,23,16,5,14,7,22},
			{20,14,22,7,14,22,24,6},
			{15,23,24,23,9,6,6,20},
			{14,8,11,14,12,12,25,10},
			{10,9,11,9,13,19,5,17},
			{8,22,20,15,12,8,22,17},
			{24,22,17,12,18,11,23,14},
			{21,25,15,16,23,14,22,22}
	};
	static double[][] TM = new double[N][N];
	static double[][] DCT = new double[N][N];
	static double[][] R = new double[N][N];
	static double[][] RTt = new double[N][N];
	static double[][] IDCT = new double[N][N];
	static int[][] quantization = new int[N][N];
	static int[][] q50 ={{16,11,10,16,24,40,51,61},
			{12,12,14,19,26,58,60,55},
			{14,13,16,24,40,57,69,56},
			{14,17,22,29,51,87,80,62},
			{18,22,37,56,68,109,103,77},
			{24,35,55,64,81,104,113,92},
			{49,64,78,87,103,121,120,101},
			{72,92,95,98,112,100,103,99}
	};
	static double[][] C = new double[N][N];
	static DecimalFormat df = new DecimalFormat("####0.000");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CompressionAndDecompression frame = new CompressionAndDecompression();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	static void generate_T(){
		for (int R = 0; R < N; R++) {
			for (int C = 0; C < N; C++) {
				if(R==0){
					T[R][C] = 1/Math.sqrt(n);
					Tt[C][R] = T[R][C] ;
				}else{
					T[R][C] = Math.sqrt((2.0/n))*Math.cos((R*Math.PI*(2*C+1))/(2*n));
					Tt[C][R] = T[R][C] ;
				}
			}
		}
	}
	
	static double[][] quantizationOfDCT(double[][] DCT,int[][] QB){
		double[][] result = new double[N][N];
		for (int R = 0; R < N; R++) {
			for (int C = 0; C < N; C++) {
				result[R][C] = Math.round(DCT[R][C] / QB[R][C]);
			}
		}
		return result;
	}
	
	static double[][] multiplyMat(double[][] matrixA, double[][] matrixB){
		double[][] result = new double[N][N];
		double sum = 0.0;
		for (int R = 0; R < N; R++) {
			for (int C = 0; C < N; C++) {
				sum = 0.0;
				for (int K = 0; K < N; K++) {
					sum += matrixA[R][K] * matrixB[K][C];
				}
				result[R][C] = sum;
			}
		}
		return result;
	}
	
	
	static void getQuantizationBlock(int qLevel){	
		
		if(qLevel == 50){
			quantization = q50;
		}
		else{
			for (int r = 0; r <n ; r++) {
				for (int c = 0; c < n ; c++) {
					if(qLevel>50){
						quantization[r][c] = (q50[r][c]*(100-qLevel)/50);
					}else{
						quantization[r][c] = (q50[r][c]*(50/qLevel));
					}
				}
			}
		}
	}
	
	public static void decompression() {
		double[][] result = new double[N][N];
		for (int R = 0; R < N; R++) {
			for (int c = 0; c < N; c++) {
				result[R][c] = C[R][c] * quantization[R][c];
			}
		}
		
		R = result;		
		RTt = multiplyMat(Tt,R);
		IDCT = multiplyMat(RTt, T);
	}

	/**
	 * Create the frame.
	 */
	public CompressionAndDecompression() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 740, 690);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lblCompressionAndDecompression = new JLabel("Compression and Decompression");
		lblCompressionAndDecompression.setFont(new Font("Gungsuh", Font.BOLD | Font.ITALIC, 27));
		lblCompressionAndDecompression.setBounds(52, -15, 587, 98);
		panel.add(lblCompressionAndDecompression);

		JLabel lblPleaseEnterThe = new JLabel("Please Enter the Quantization Level");
		lblPleaseEnterThe.setBounds(31, 80, 240, 14);
		panel.add(lblPleaseEnterThe);

		quantizationLevel = new JTextField();
		quantizationLevel.setBounds(229, 77, 86, 20);
		panel.add(quantizationLevel);
		quantizationLevel.setColumns(10);
		JTextArea textAreaQ = new JTextArea();
		textAreaQ.setBounds(31, 140, 240, 212);
		panel.add(textAreaQ);

		JLabel lblQuantizationBlock = new JLabel("Quantization Block");
		lblQuantizationBlock.setBounds(56, 115, 115, 14);
		panel.add(lblQuantizationBlock);

		JLabel lblToBeCompressed = new JLabel("To be Compressed");
		lblToBeCompressed.setBounds(491, 110, 115, 14);
		panel.add(lblToBeCompressed);

		JTextArea textAreaM = new JTextArea();
		textAreaM.setBounds(445, 140, 240, 212);
		panel.add(textAreaM);

		JTextArea textAreaCompressed = new JTextArea();
		textAreaCompressed.setBounds(31, 402, 240, 212);
		panel.add(textAreaCompressed);

		JLabel lblCompressedFile = new JLabel("Compressed File");
		lblCompressedFile.setBounds(56, 377, 115, 14);
		panel.add(lblCompressedFile);

		JTextArea textAreaDecompressed = new JTextArea();
		textAreaDecompressed.setBounds(445, 402, 240, 212);
		panel.add(textAreaDecompressed);

		JLabel lblDecompressedFile = new JLabel("Decompressed File");
		lblDecompressedFile.setBounds(470, 377, 115, 14);
		panel.add(lblDecompressedFile);

		JButton btnDecompress = new JButton("Decompress");
		btnDecompress.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				decompression();
				String decom = "";
				for (int r = 0; r < N; r++) {
					for (int c = 0; c < N; c++) {
						decom += Math.round(IDCT[r][c]) + " ";
					}
					decom += "\n";
				}
				
				textAreaDecompressed.setText(decom);
				
			}
		});
		btnDecompress.setBounds(299, 490, 116, 20);
		panel.add(btnDecompress);

		JButton btnSubmit = new JButton("Compress");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				generate_T();//generates both T and Tt
				int q = Integer.parseInt(quantizationLevel.getText());
				getQuantizationBlock(q);
				
				TM = multiplyMat(T, M);
				DCT = multiplyMat(TM, Tt);
				C = quantizationOfDCT(DCT, quantization);
				String Q = "";
				String Comp = "";
				String Message = "";
				for (int r = 0; r < N; r++) {
					for (int c = 0; c < N; c++) {
						Q += Math.round(quantization[r][c]) + " ";
					}
					Q += "\n";
				}
				for (int r = 0; r < N; r++) {
					for (int c = 0; c < N; c++) {
						Comp +=  Math.round(C[r][c]) + " ";
					}
					Comp += "\n";
				}
				for (int r = 0; r < N; r++) {
					for (int c = 0; c < N; c++) {
						Message +=  Math.round(M[r][c]) + " ";
					}
					Message += "\n";
				}
				
				textAreaQ.setText(Q);
				textAreaM.setText(Message);
				textAreaCompressed.setText(Comp);
				
			}
		});
		btnSubmit.setBounds(299, 210, 116, 23);
		panel.add(btnSubmit);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setBounds(314, 591, 89, 23);
		panel.add(btnExit);

		
	}
}
