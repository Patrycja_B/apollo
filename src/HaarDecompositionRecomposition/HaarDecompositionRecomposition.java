package HaarDecompositionRecomposition;

import java.text.DecimalFormat;

public class HaarDecompositionRecomposition {
	static int size=8;
	static double [][] PixelArray={{230,127,127,200,200,127,127,230},	
									{127,230,127,230,230,127,230,127},{127,127,230,230,230,230,127,127},{200,230,230,255,255,230,230,230},
									{200,230,230,255,255,230,230,230},{127,127,230,230,230,230,127,127},{127,230,127,230,230,127,230,127},
									{230,127,127,200,200,127,127,230}};
	static double [][] tempArray=new double[size][size];
	static DecimalFormat df=new DecimalFormat("####0.00");

	public static void main(String[] args){
		printMatrix(PixelArray);
		decompositionAndRecomposition();

	}

	static void decompositionAndRecomposition(){
		System.out.println();	
		System.out.println("*********************Decomposition on Rows***************************");
		int index=size;
		int j,k;
		int offset;
		int step=1;
		while(index>=2){ //decompositon on rows
			offset=(index/2);
			for(k=0;k<size;k++){
				for(j=0;j<index;j+=2){
					double avg=(PixelArray[k][j] + PixelArray [k][j+1])/2.0;
					double diff=PixelArray[k][j] - avg;
					//put these elements in temp array
					tempArray [k] [j/2]=avg;
					tempArray [k] [offset + (j/2)]=diff;
				}

			}
			// Store decomposed data in pixelarray for next decomposition step.
			
			
			for(k=0;k<size;k++)
				for(j=0;j<index;j++)
					PixelArray[k][j] = tempArray[k][j];
			
			index/=2;
			step++;
		}
		printMatrix(PixelArray); // rows decomposition
		
		System.out.println();
		System.out.println("*******************Decomposition on Columns*************************");
		index=size;
		step=1;
		while(index>=2){
			offset=(index/2);
			for(j=0;j<size;j++){
				for(k=0;k<index;k+=2){
					double avg=(tempArray[k] [j] + tempArray [k+1] [j])/2.0;
					double diff=tempArray[k] [j] - avg;
					PixelArray [k/2] [j]=avg;
					PixelArray [offset + (k/2)] [j]=diff;
				}
			}
			for(j=0;j<size;j++) 
				for(k=0;k<index;k++)
				tempArray[k][j] = PixelArray[k][j];
			index/=2;
			step++;
		}
		printMatrix(PixelArray);   // columns decomposition
		
		System.out.println();
		System.out.println("************************Recomposition on coulmns*********************");
		 index = 1;
	     step = 1;

	     while (index < size)
	     {
	       for ( j = 0; j < size; j++)
	       {
	         for (int i = 0; i < index; i++)
	         {
	           PixelArray[(2 * i)][j] = (tempArray[i][j] + tempArray[(index + i)][j]);
	           PixelArray[(2 * i + 1)][j] = (tempArray[i][j] - tempArray[(index + i)][j]);
	         }
	       }

	       index *= 2;
	       for ( j = 0; j < size; j++)
	       for ( k = 0; k < index; k++) 
	       tempArray[k][j] = PixelArray[k][j];
	    
	       step++;
	     }
	     printMatrix(PixelArray);   //columns recomposition
	     index=1;
	     step=1;
	     System.out.println();
	     System.out.println("************************Recomposition on Rows*********************");
	     while (index < size)
	      {
	        for (int i = 0; i < size; i++)
	        {
	          for ( j = 0; j < index; j++)
	          {
	            tempArray[i][(2 * j)] = (PixelArray[i][j] + PixelArray[i][(index + j)]);
	            tempArray[i][(2 * j + 1)] = (PixelArray[i][j] - PixelArray[i][(index + j)]);
	          }
	        }

	       index *= 2;
	        for (k = 0; k < size; k++)
	        for (j = 0; j < index; j++)
	        PixelArray[k][j] = tempArray[k][j];
	        step++;
	      }
	     printMatrix(PixelArray);  // returns original matrix  rows recomposition

		}
		
	
	static void printMatrix(double[][] result){
		System.out.println();
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[0].length; j++) {
				System.out.print((int)result[i][j]+"\t"); // remove int for double numbers
				
			}
			System.out.println(" ");
			
		}
	}

}
