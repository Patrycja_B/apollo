package shuntingYard;

import java.util.Iterator;
import java.util.Stack;

public class ShuntingYardAlgorithm {

	public static void main(String[] args) {
		//System.out.println("Your equation returns : " +ShuntingYard("5 + 3 * 6 - ( 5 / 3 ) + 7"));;
		String infix = "2.9 + ( 3 * ( 8 - 4 ) )";
        System.out.printf("infix:   %s%n", infix);
        System.out.printf("postfix: %s%n", infixToPostfix(infix));
        System.out.println("Equation result : " + calculatePostfix(infixToPostfix(infix)));
	}
	 static String infixToPostfix(String infix) {
	        /* To find out the precedence, we take the index of the
	           token in the ops string and divide by 2 (rounding down). 
	           This will give us: 0, 0, 1, 1, 2 */
	        final String operators = "-+/*^";
	 
	        StringBuilder sb = new StringBuilder();
	        Stack<Integer> s = new Stack<>();
	        
	        for (String token : infix.split("\\s")) {
	            if (token.isEmpty())
	                continue;
	            char c = token.charAt(0);
	            int idx = operators.indexOf(c);
	 
	            // check for operator
	            if (idx != -1) {
	                if (s.isEmpty())
	                    s.push(idx);
	 
	                else {
	                    while (!s.isEmpty()) {
	                        int prec2 = s.peek() / 2;
	                        int prec1 = idx / 2;
	                        if (prec2 > prec1 || (prec2 == prec1 && c != '^'))
	                            sb.append(operators.charAt(s.pop())).append(' ');
	                        else break;
	                    }
	                    s.push(idx);
	                }
	            } 
	            else if (c == '(') {
	                s.push(-2); // -2 stands for '('
	            } 
	            else if (c == ')') {
	                // until '(' on stack, pop operators.
	                while (s.peek() != -2)
	                    sb.append(operators.charAt(s.pop())).append(' ');
	                s.pop();
	            }
	            else {
	                sb.append(token).append(' ');
	            }
	        }
	        while (!s.isEmpty())
	            sb.append(operators.charAt(s.pop())).append(' ');
	        return sb.toString();
	    }
	 
	 static String calculatePostfix(String postfixParam)
	 {
		Stack<String> numberStack = new Stack<String>();
		for (String token : postfixParam.split("\\s")) {
			if(token.equals("+"))
			{
				System.out.println("Addition: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue + secondValue;
				numberStack.push(Double.toString(result));
				
			}
			else if(token.equals("-"))
			{
				System.out.println("Subtraction: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue - secondValue;
				numberStack.push(Double.toString(result));
			}
			else if(token.equals("/"))
			{
				System.out.println("Division: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue / secondValue;
				numberStack.push(Double.toString(result));
			}
			else if(token.equals("*"))
			{
				System.out.println("Multiplication: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue * secondValue;
				numberStack.push(Double.toString(result));
			}
			else if(token.equals("^"))
			{
				System.out.println("Power : ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = Math.pow(firstValue, secondValue);
				numberStack.push(Double.toString(result));
			}
			else
			{
				System.out.println(token);
				numberStack.push(token);
			}
		}
		return numberStack.pop();
		 
	 }
	 

}
