package shuntingYard;
import java.util.Stack;

public class Shunting_Yard_Algorithm_Ell_Version {

	public static void main(String[] args) {
		// System.out.println("Your equation returns : " +ShuntingYard("5 + 3 * 6 - ( 5
		// / 3 ) + 7"));;
		//String infix = "-2.9 + ( -0.8438539587324921 +  -0.6358599286615808 )";
		String infix = "ln ( 1 + 2 + exp ( 1 ) ) +  exp ( 1 )"; //this format works
		allFunctions(infix);
	}
	static String allFunctions(String inputParam) {
		String result = inputParam;
		result = removeUnary(result);
		//result = removeCos(result);
		//result = removeSin(result);
		//result = removeTan(result);
		result = removeExp(result);
		result = removeLn(result);
		result = removeLog(result);
		result = infixToPostfix(result);
		//System.out.println("Result postfix is " + result);
		result = calculatePostfix(result);
		System.out.println("Result : "+ result);
		return result;
	}
	static String infixToPostfix(String infix) {
		/*
		 * To find out the precedence, we take the index of the token in the ops string
		 * and divide by 2 (rounding down). This will give us: 0, 0, 1, 1, 2
		 */
		final String operators = "-+/*^";

		StringBuilder sb = new StringBuilder();
		Stack<Integer> s = new Stack<>();

		for (String token : infix.split("\\s")) {
			if (token.isEmpty())
				continue;
			char c = token.charAt(0);
			int idx = operators.indexOf(c);

			if (token.length() > 1 && c =='-')
			{
				sb.append("0 ");
			}
			// check for operator
			if (idx != -1) {
				if (s.isEmpty())
					s.push(idx);

				else {
					while (!s.isEmpty()) {
						int prec2 = s.peek() / 2;
						int prec1 = idx / 2;
						if (prec2 > prec1 || (prec2 == prec1 && c != '^'))
							sb.append(operators.charAt(s.pop())).append(' ');
						else
							break;
					}
					s.push(idx);
				}
			} else if (c == '(') {
				s.push(-2); // -2 stands for '('
			} else if (c == ')') {
				// until '(' on stack, pop operators.
				while (s.peek() != -2)
					sb.append(operators.charAt(s.pop())).append(' ');
				s.pop();
			} else {
				sb.append(token).append(' ');
			}
		}
		while (!s.isEmpty())
			sb.append(operators.charAt(s.pop())).append(' ');
		return sb.toString();
	}

	static String calculatePostfix(String postfixParam) {
		Stack<String> numberStack = new Stack<String>();
		for (String token : postfixParam.split("\\s")) {
			if (token.equals("+")) {
				System.out.println("Addition: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue + secondValue;
				numberStack.push(Double.toString(result));

			} else if (token.equals("-")) {
				System.out.println("Subtraction: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue - secondValue;
				numberStack.push(Double.toString(result));
		//My work on the negative values		
		/*	} else if (token.equals("-")) {
				System.out.println("Subtraction: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				if(numberStack.peek() != null ) { // && (numberStack.peek())

					double firstValue = Double.parseDouble(numberStack.pop());
					double result = firstValue - secondValue;
					numberStack.push("" + (result));
				} else {
					numberStack.push("" + (-secondValue));
				}
			} */	
			} else if (token.equals("/")) {
				System.out.println("Division: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue / secondValue;
				numberStack.push(Double.toString(result));
			} else if (token.equals("*")) {
				System.out.println("Multiplication: ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = firstValue * secondValue;
				numberStack.push(Double.toString(result));
			} else if (token.equals("^")) {
				System.out.println("Power : ");
				double secondValue = Double.parseDouble(numberStack.pop());
				double firstValue = Double.parseDouble(numberStack.pop());
				double result = Math.pow(firstValue, secondValue);
				numberStack.push(Double.toString(result));
			} else {
				System.out.println(token);
				numberStack.push(token);
			}
		}
		return numberStack.pop();

	}
	
	static String removeUnary(String inputParam) {
		StringBuilder sb = new StringBuilder();
		//Run through String until it reaches a negative number and inserts 0 - before that number
		for(int i =0;i<inputParam.length();i++) {
			if((inputParam.charAt(i)=='-') && (Character.isDigit(inputParam.charAt(i+1)))) {
				sb.append("0 - ");
			}
			else
			{
				sb.append(inputParam.charAt(i));
			}
		}
		return sb.toString();
	}

	static String removeCos(String inputParam) {
		int cosStart =0;
		int cosEnd = 0;
		
		while(inputParam.contains("cos"))
		{
			StringBuilder newString = new StringBuilder();
			cosStart =0;
			cosEnd = 0;
			int count = 0;
			//Sets cosStart to point of first bracker
			cosStart = inputParam.indexOf("cos")+4;
			cosEnd =cosStart;
			//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
			do {
				if(inputParam.charAt(cosEnd) =='(')
				{
					count++;
				}
				else if(inputParam.charAt(cosEnd)==')')
				{
					count--;
				}
				cosEnd++;
			}
			while (count !=0);
			
			//Changes 
			String result = infixToPostfix(removeCos(inputParam.substring(cosStart, cosEnd)));
			result = calculatePostfix(result);
			
			
			//Caclulate the actual cos value
			double cosValue = Double.parseDouble(result);
			cosValue = Math.cos(cosValue);
			result = Double.toString(cosValue);
			
			
			
			//Create new String
			newString.append(inputParam.substring(0, cosStart - 4));
			newString.append(result);
			newString.append(inputParam.substring(cosEnd, inputParam.length()));
			inputParam = newString.toString();
			System.out.println("New String: " + inputParam);
		}
		return inputParam;
	}
	
	static String removeSin(String inputParam) {
		int cosStart =0;
		int cosEnd = 0;
		
		while(inputParam.contains("sin"))
		{
			StringBuilder newString = new StringBuilder();
			cosStart =0;
			cosEnd = 0;
			int count = 0;
			//Sets cosStart to point of first bracker
			cosStart = inputParam.indexOf("sin")+4;
			cosEnd =cosStart;
			//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
			do {
				if(inputParam.charAt(cosEnd) =='(')
				{
					count++;
				}
				else if(inputParam.charAt(cosEnd)==')')
				{
					count--;
				}
				cosEnd++;
			}
			while (count !=0);
			
			//Changes 
			String result = infixToPostfix(removeCos(inputParam.substring(cosStart, cosEnd)));
			result = calculatePostfix(result);
			
			
			//Caclulate the actual cos value
			double cosValue = Double.parseDouble(result);
			cosValue = Math.sin(cosValue);
			result = Double.toString(cosValue);
			
			
			
			//Create new String
			newString.append(inputParam.substring(0, cosStart - 4));
			newString.append(result);
			newString.append(inputParam.substring(cosEnd, inputParam.length()));
			inputParam = newString.toString();
			System.out.println("New String: " + inputParam);
		}
		return inputParam;
	}
	
	static String removeTan(String inputParam) {
		int cosStart =0;
		int cosEnd = 0;
		
		while(inputParam.contains("tan"))
		{
			StringBuilder newString = new StringBuilder();
			cosStart =0;
			cosEnd = 0;
			int count = 0;
			//Sets cosStart to point of first bracket
			cosStart = inputParam.indexOf("tan")+4;
			cosEnd =cosStart;
			//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
			do {
				if(inputParam.charAt(cosEnd) =='(')
				{
					count++;
				}
				else if(inputParam.charAt(cosEnd)==')')
				{
					count--;
				}
				cosEnd++;
			}
			while (count !=0);
			
			//Changes 
			String result = infixToPostfix(removeCos(inputParam.substring(cosStart, cosEnd)));
			result = calculatePostfix(result);
			
			
			//Caclulate the actual tan value
			double cosValue = Double.parseDouble(result);
			cosValue = Math.tan(cosValue);
			result = Double.toString(cosValue);
			
			
			
			//Create new String
			newString.append(inputParam.substring(0, cosStart - 4));
			newString.append(result);
			newString.append(inputParam.substring(cosEnd, inputParam.length()));
			inputParam = newString.toString();
			System.out.println("New String: " + inputParam);
		}
		return inputParam;
	}

		
//-----------------------------------------------------------------------------------------------------------------------------------------------------------
		static String removeLn(String inputParam) {
			int lnStart =0;
			int lnEnd = 0;
			
			while(inputParam.contains("ln"))
			{
				StringBuilder newString = new StringBuilder();
				lnStart =0;
				lnEnd = 0;
				int count = 0;
				//Sets lnStart to point of first bracket
				lnStart = inputParam.indexOf("ln")+3;
				lnEnd =lnStart;
				//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
				do {
					if(inputParam.charAt(lnEnd) =='(')
					{
						count++;
					}
					else if(inputParam.charAt(lnEnd)==')')
					{
						count--;
					}
					lnEnd++;
				}
				while (count !=0);
				
				//Changes 
				String result = infixToPostfix(removeLn(inputParam.substring(lnStart, lnEnd)));
				result = calculatePostfix(result);
				
				
				//Caclulate the actual ln value
				double lnValue = Double.parseDouble(result);
				lnValue = Math.log(lnValue);
				result = Double.toString(lnValue);
				
				
				
				//Create new String
				newString.append(inputParam.substring(0, lnStart - 3));
				newString.append(result);
				newString.append(inputParam.substring(lnEnd, inputParam.length()));
				inputParam = newString.toString();
				System.out.println("New String: " + inputParam);
			}
			return inputParam;
		}
		
		static String removeExp(String inputParam) {
			int expStart =0;
			int expEnd = 0;
			
			while(inputParam.contains("exp"))
			{
				StringBuilder newString = new StringBuilder();
				expStart =0;
				expEnd = 0;
				int count = 0;
				//Sets expStart to point of first bracket
				expStart = inputParam.indexOf("exp")+4;
				expEnd =expStart;
				//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
				do {
					if(inputParam.charAt(expEnd) =='(')
					{
						count++;
					}
					else if(inputParam.charAt(expEnd)==')')
					{
						count--;
					}
					expEnd++;
				}
				while (count !=0);
				
				//Changes 
				String result = infixToPostfix(removeExp(inputParam.substring(expStart, expEnd)));
				result = calculatePostfix(result);
				
				
				//Calculate the actual exp value
				double expValue = Double.parseDouble(result);
				expValue = Math.exp(expValue);
				result = Double.toString(expValue);
				
				
				
				//Create new String
				newString.append(inputParam.substring(0, expStart - 4));
				newString.append(result);
				newString.append(inputParam.substring(expEnd, inputParam.length()));
				inputParam = newString.toString();
				System.out.println("New String: " + inputParam);
			}
			return inputParam;
		}
		
		static String removeLog(String inputParam) {
			int logStart =0;
			int logEnd = 0;
			
			while(inputParam.contains("log"))
			{
				StringBuilder newString = new StringBuilder();
				logStart =0;
				logEnd = 0;
				int count = 0;
				//Sets lnStart to point of first bracket
				logStart = inputParam.indexOf("log")+4;
				logEnd =logStart;
				//Runs once to get count to +1 and then exits when the count of brackets becomes zero again
				do {
					if(inputParam.charAt(logEnd) =='(')
					{
						count++;
					}
					else if(inputParam.charAt(logEnd)==')')
					{
						count--;
					}
					logEnd++;
				}
				while (count !=0);
				
				//Changes 
				String result = infixToPostfix(removeLog(inputParam.substring(logStart, logEnd)));
				result = calculatePostfix(result);
				
				
				//Caclulate the actual log value
				double logValue = Double.parseDouble(result);
				logValue = Math.log10(logValue);
				result = Double.toString(logValue);
				
				
				
				//Create new String
				newString.append(inputParam.substring(0, logStart - 4));
				newString.append(result);
				newString.append(inputParam.substring(logEnd, inputParam.length()));
				inputParam = newString.toString();
				System.out.println("New String: " + inputParam);
			}
			return inputParam;
		}
//------------------------------------------------------------------------------------------------------------------------------------------------------------

	}
